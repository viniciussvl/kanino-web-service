package ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.CategoryDAO;
import dao.CustomerDAO;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import models.CategoryModel;
import models.CustomerModel;


@Path("/categorias")
public class Category {

    @GET
    @Produces("application/json")
    public String getProducts() {
        List<CategoryModel> list;

        CategoryDAO dao = new CategoryDAO();
        list = dao.getCategories();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);
    }
    
    
    
   
}
