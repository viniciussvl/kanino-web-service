package ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.CustomerDAO;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import models.CustomerModel;

@Path("/cliente")
public class Customer {

    @POST
    @Path("login")
    @Produces("application/json")
    public String login(@FormParam("email") String email, @FormParam("password") String password) {

        CustomerModel c = new CustomerModel();
        c.setEmail(email);
        c.setPassword(password);

        CustomerDAO dao = new CustomerDAO();
        String where = "WHERE emailCliente = ? AND senhaCliente = ?";
        c = dao.search(c, where);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(c);

    }

    @POST
    @Path("registrar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(CustomerModel c) {

        Long output = -1L;
        CustomerDAO dao = new CustomerDAO();
        try {
            output = dao.register(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(output).build();

    }

    @POST
    @Path("editar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response edit(CustomerModel c) {

        Long output = -1L;
        CustomerDAO dao = new CustomerDAO();
        try {
            output = dao.edit(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(output).build();

    }

    @POST
    @Path("alterar-senha")
    @Produces("application/json")
    public Response changePassword(@FormParam("idCustomer") Long idCustomer, @FormParam("password") String password) {

        CustomerModel c = new CustomerModel();
        c.setId(idCustomer);
        c.setPassword(password);

        Long output = -1L;
        CustomerDAO dao = new CustomerDAO();
        try {
            output = dao.changePassword(c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(output).build();

    }

    @POST
    @Path("recuperar-senha")
    @Produces("application/json; charset=UTF-8")
    public String login(@FormParam("email") String email) throws SQLException {

        String result = null;

        CustomerModel c = new CustomerModel();
        c.setEmail(email);

        CustomerDAO dao = new CustomerDAO();
        result = dao.forgotPassword(c);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(result);

    }
}
