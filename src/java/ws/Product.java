package ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.CustomerDAO;
import dao.ProductDAO;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import models.CustomerModel;
import models.ProductModel;

@Path("/produtos")
public class Product {

    private Object Response;

    @GET
    @Produces("application/json; charset=UTF-8")
    public String getProducts() throws SQLException {
        List<ProductModel> list;

        ProductDAO dao = new ProductDAO();
        list = dao.getProducts();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);
    }

    @GET
    @Produces("application/json; charset=UTF-8")
    @Path("{id}")
    public String getProduct(@PathParam("id") Integer productId) {

        ProductModel p = new ProductModel();
        p.setId(productId);

        ProductDAO dao = new ProductDAO();
        p = dao.getProductById(p);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(p);
    }

    @GET
    @Path("categoria/{id}")
    @Produces("application/json; charset=UTF-8")
    public String getProductsByCategory(@PathParam("id") Integer categoryId) throws Exception {

        List<ProductModel> list;

        ProductModel p = new ProductModel();
        p.setCategoryId(categoryId);

        ProductDAO dao = new ProductDAO();
        list = dao.getProductsByCategory(p);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);

    }

    @GET
    @Path("imagem/{id}")
    @Produces("image/png")
    public byte[] getProductImage(@PathParam("id") long imageId) throws Exception {
        byte[] output = null;

        try {
            ProductDAO dao = new ProductDAO();
            output = dao.getProductImage(imageId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return output;
    }

    @GET
    @Path("buscar/{parameter}")
    @Produces("application/json; charset=UTF-8")
    public String search(@PathParam("parameter") String parameter) throws SQLException {
        List<ProductModel> list;

        ProductDAO dao = new ProductDAO();
        list = dao.search(parameter);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);
    }

}
