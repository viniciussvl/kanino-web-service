/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.AddressDAO;
import dao.CustomerDAO;
import dao.OrderDAO;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.AddressModel;
import models.OrderModel;

/**
 *
 * @author victor.maciel
 */
@Path("/endereco")
public class Address {

    @POST
    @Path("cadastrar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(AddressModel a) {

        Long output = -1L;
        AddressDAO dao = new AddressDAO();
        try {
            output = dao.create(a);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(output).build();

    }

    @POST
    @Path("remover")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@FormParam("id") Long id) {

        Integer output = 0;

        AddressDAO dao = new AddressDAO();
        try {
            output = dao.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(output).build();

    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String search(@PathParam("id") Long id) throws SQLException {

        List<AddressModel> list;

        AddressDAO dao = new AddressDAO();
        list = dao.search(id);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);

    }
}
