package ws;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.OrderDAO;
import dao.ProductDAO;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import models.OrderItemModel;
import models.OrderModel;
import models.ProductModel;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/pedidos")
public class Order {

    @GET
    @Produces("application/json; charset=UTF-8")
    @Path("{id}")
    public String orders(@PathParam("id") Long id) {

        List<OrderModel> list;

        OrderModel o = new OrderModel();
        o.setIdCustomer(id);

        OrderDAO dao = new OrderDAO();
        list = dao.orders(o);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);

    }

    @GET
    @Produces("application/json; charset=UTF-8")
    @Path("pedido/{id}")
    public String order(@PathParam("id") Long id) throws Exception {

        List<OrderItemModel> list;

        OrderDAO dao = new OrderDAO();
        list = dao.getOrder(id);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(list);

    }

    @POST
    @Path("checkout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkout(OrderModel o) throws Exception {

        OrderDAO dao = new OrderDAO();
        Long orderId = -1L;
        try {
            orderId = dao.registerOrder(o);

            List products = o.getProducts();

            for (int i = 0; i < products.size(); i++) {
                dao.registerOrderItem((OrderItemModel) products.get(i), orderId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.ok(orderId).build();
    }

}
