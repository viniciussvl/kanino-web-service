/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyConnection {

    private static final String database = "jdbc:sqlserver://kanino-pi.database.windows.net:1433;DatabaseName=kanino";
    private static final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String username = "TSI@kanino-pi.database.windows.net";
    private static final String password = "SistemasInternet123";
    private static Connection c = null;

    public static Connection getConnection() {

        if (c == null) {
            try {
                Class.forName(driver);
                c = DriverManager.getConnection(database, username, password);
            } catch (ClassNotFoundException ex) {
                System.out.println("Não encontrou o driver");
            } catch (SQLException ex) {
                System.out.println("Erro ao conectar: " + ex.getMessage());
            }
        }
        return c;
    }

    public static PreparedStatement getPreparedStatement(String sql) {

        if (c == null) {
            c = getConnection();
        }
        try {
            return c.prepareStatement(sql);
        } catch (SQLException e) {
            System.out.println("Erro de sql: "
                    + e.getMessage());
        }
        return null;
    }

    static PreparedStatement getPreparedStatement(String sql, int RETURN_GENERATED_KEYS) {
        if (c == null) {
            c = getConnection();
        }
        try {
            return c.prepareStatement(sql, RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            System.out.println("Erro de sql: "
                    + e.getMessage());
        }
        return null;
    }

}
