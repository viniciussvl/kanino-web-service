package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.CustomerModel;

public class CustomerDAO {

    private String defaultSelect = "idCliente, nomeCompletoCliente, emailCliente, senhaCliente, CPFCliente, celularCliente, telComercialCliente, telResidencialCliente, dtNascCliente, recebeNewsLetter";

    public CustomerModel search(CustomerModel customer, String where) {
        String sql = "SELECT " + this.defaultSelect + " FROM cliente " + where;
        CustomerModel result = null;

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        try {

            pst.setString(1, customer.getEmail());
            pst.setString(2, customer.getPassword());
            ResultSet res = pst.executeQuery();

            if (res.next()) {
                result = new CustomerModel();
                result.setId(res.getLong("idCliente"));
                result.setEmail(res.getString("emailCliente"));
                result.setName(res.getString("nomeCompletoCliente"));
                result.setCpf(res.getString("CPFCliente"));
                result.setCell_phone(res.getString("celularCliente"));
                result.setCommercial_phone(res.getString("telComercialCliente"));
                result.setResidencial_phone(res.getString("telResidencialCliente"));
                result.setSend_newsletter(res.getString("recebeNewsLetter"));
                result.setBirth(res.getString("dtNascCliente"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return result;

    }

    public Long register(CustomerModel customer) throws SQLException {
        String sql = "INSERT INTO cliente (nomeCompletoCliente, emailCliente, senhaCliente, CPFCliente, celularCliente, telComercialCliente, telResidencialCliente, dtNascCliente, recebeNewsLetter) VALUES(?,?,?,?,?,?,?,?,?)";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setString(1, customer.getName());
            pst.setString(2, customer.getEmail());
            pst.setString(3, customer.getPassword());
            pst.setString(4, customer.getCpf());
            pst.setString(5, customer.getCell_phone());
            pst.setString(6, customer.getCommercial_phone());
            pst.setString(7, customer.getResidencial_phone());
            pst.setString(8, customer.getBirth());
            pst.setString(9, customer.getSend_newsletter());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Não foi possível cadastrar o cliente!");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;

    }

    public Long edit(CustomerModel customer) throws SQLException {
        String sql
                = "UPDATE Cliente SET nomeCompletoCliente = ?,emailCliente = ?,celularCliente = ?,telComercialCliente = ?,telResidencialCliente = ?,recebeNewsLetter = ? WHERE idCliente = ?;";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setString(1, customer.getName());
            pst.setString(2, customer.getEmail());
            pst.setString(3, customer.getCell_phone());
            pst.setString(4, customer.getCommercial_phone());
            pst.setString(5, customer.getResidencial_phone());
            pst.setString(6, customer.getSend_newsletter());
            pst.setLong(7, customer.getId());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = (Long) customer.getId();
                } else {
                    throw new SQLException("Não foi possível atualizar o cliente!!!!");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

    public String forgotPassword(CustomerModel customer) throws SQLException {
        String alphaNum = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String randomPassword = "";
        String result = null;
        Random random = new Random();

        for (int i = 0; i < 8; i++) {
            int numRnd = random.nextInt(alphaNum.length());
            randomPassword += alphaNum.substring(numRnd, numRnd + 1);
        }

        String sql = "UPDATE cliente SET senhaCliente = ? where emailCliente = ?";
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql);

        try {
            pst.setString(2, customer.getEmail());
            pst.executeUpdate();

            Properties props = new Properties();

            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("kaninows@gmail.com", "vns102040");
                }
            });

            session.setDebug(true);
            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("kaninows@gmail.com")); //Remetente

                Address[] toUser = InternetAddress //Destinatário(s)
                        .parse(customer.getEmail());
                message.setRecipients(Message.RecipientType.TO, toUser);
                message.setSubject("Recuperação de Senha - Kanino");//Assunto

                message.setContent("<center><img src='https://i.imgur.com/qxmKrx1.png' width='100' alt='kanino'></center>"
                        + "Sua senha foi alterada com sucesso. "
                        + "Para fazer login no aplicativo utilize os seguintes dados:<br><br>"
                        + "E-mail: <strong>" + customer.getEmail() + "</strong><br>"
                        + "Senha: <strong>" + randomPassword + "</strong>.<br><br>"
                        + "Assim que fizer login no aplicativo altere sua senha na área do cliente. Qualquer dúvida entre em contato com o suporte da loja pelo e-mail contato@kanino.com.br", "text/html; charset=utf-8");

                Transport.send(message);
                result = "alterado";

            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            result = "erro";
        }

        return result;
    }

    public Long changePassword(CustomerModel customer) throws SQLException {
        String sql = "UPDATE Cliente SET senhaCliente = ? WHERE idCliente = ?";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setString(1, customer.getPassword());
            pst.setLong(2, customer.getId());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = (Long) customer.getId();
                } else {
                    throw new SQLException("Não foi possível alterar a senha do cliente!");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

}
