/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.AddressModel;

/**
 *
 * @author victor.maciel
 */
public class AddressDAO {

    public Long create(AddressModel a) throws SQLException {
        String sql = "INSERT INTO Endereco (idCliente, nomeEndereco, logradouroEndereco, numeroEndereco, CEPEndereco, complementoEndereco, cidadeEndereco, paisEndereco, UFEndereco) VALUES(?,?,?,?,?,?,?,?,?   )";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setLong(1, a.getId_customer());
            pst.setString(2, a.getAddress_name());
            pst.setString(3, a.getStreet_name());
            pst.setString(4, a.getAddress_number());
            pst.setString(5, a.getZip_code());
            pst.setString(6, a.getComplement());
            pst.setString(7, a.getCity());
            pst.setString(8, a.getCountry());
            pst.setString(9, a.getUf());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Não foi possível cadastrar o endereço!!!!");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

    public Long update(AddressModel a) throws SQLException {
        String sql = "INSERT INTO Endereco (idCliente, nomeEndereco, logradouroEndereco, numeroEndereco, CEPEndereco, complementoEndereco, cidadeEndereco, paisEndereco, UFEndereco) VALUES(?,?,?,?,?,?,?,?,?   )";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setLong(1, a.getId_customer());
            pst.setString(2, a.getAddress_name());
            pst.setString(3, a.getStreet_name());
            pst.setString(4, a.getAddress_number());
            pst.setString(5, a.getZip_code());
            pst.setString(6, a.getComplement());
            pst.setString(7, a.getCity());
            pst.setString(8, a.getCountry());
            pst.setString(9, a.getUf());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Não foi possível cadastrar o endereço!!!!");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

    public Integer delete(Long id) throws SQLException {
        String sql = "DELETE FROM Endereco where idEndereco = ?";
        Integer output = 0;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql);
        try {
            pst.setLong(1, id);
            int affectedRows = pst.executeUpdate();
            if (affectedRows > 0) {
                output = 1;
            } else {
                throw new SQLException("Não foi possível deletar o endereço!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

    public List<AddressModel> search(Long idCliente) throws SQLException {
        String sql = "SELECT * FROM endereco WHERE idCliente = ?";
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        List<AddressModel> result = new ArrayList<>();
        pst.setLong(1, idCliente);

        ResultSet res = pst.executeQuery();

        while (res.next()) {
            AddressModel item = new AddressModel();
            item.setId_address(res.getInt("idEndereco"));
            item.setAddress_name(res.getString("nomeEndereco"));
            item.setStreet_name(res.getString("logradouroEndereco"));
            item.setAddress_number(res.getString("numeroEndereco"));
            item.setZip_code(res.getString("CEPEndereco"));
            item.setComplement(res.getString("complementoEndereco"));
            item.setCity(res.getString("cidadeEndereco"));
            item.setCountry(res.getString("paisEndereco"));
            item.setUf(res.getString("UFEndereco"));

            result.add(item);

        }

        return result;
    }

}
