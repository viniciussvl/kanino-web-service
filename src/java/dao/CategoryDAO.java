package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.CategoryModel;

public class CategoryDAO {

    private String defaultSelect = "idCategoria, nomeCategoria, descCategoria";
    
    public List<CategoryModel> getCategories() {

        String sql = "SELECT " + this.defaultSelect + " FROM categoria";
        List<CategoryModel> result = new ArrayList<CategoryModel>();

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        try {

            ResultSet res = pst.executeQuery();
            while (res.next()) {
                CategoryModel item = new CategoryModel();
                item.setId(res.getLong("idCategoria"));
                item.setName(res.getString("nomeCategoria"));
                item.setDescricao(res.getString("descCategoria"));
                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return result;

    }
    
    
}
