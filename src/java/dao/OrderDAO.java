package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.OrderItemModel;
import models.OrderModel;

public class OrderDAO {

    private String defaultSelect = "idPedido, idCliente, idStatus, dataPedido, idTipoPagto, idEndereco, idAplicacao";

    public Long registerOrderItem(OrderItemModel orderItem, Long orderId) throws SQLException {
        String sql = "INSERT INTO itemPedido (idProduto, idPedido, qtdProduto, precoVendaItem) VALUES(?,?,?,?)";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setLong(1, orderItem.getId());
            pst.setLong(2, orderId);
            pst.setLong(3, orderItem.getQtd());
            pst.setString(4, orderItem.getPrice());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    this.changeStockValue(orderItem.getId(), orderItem.getQtd());
                    output = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Não foi possível cadastrar o item do pedido.");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;

    }

    public Long changeStockValue(Long productId, Integer orderQtd) throws SQLException {
        String sql = "update estoque set qtdProdutoDisponivel = qtdProdutoDisponivel - ? where idProduto = ?";
        Long output = -1L;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql);
        try {
            pst.setInt(1, orderQtd);
            pst.setLong(2, productId);

            if (pst.executeUpdate() > 0) {
                output = 1L;
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;
    }

    public Long registerOrder(OrderModel order) throws SQLException {
        String sql = "INSERT INTO pedido (idCliente, idStatus, dataPedido, idTipoPagto, idEndereco, idAplicacao) VALUES(?,?,?,?,?,?)";
        Long output = null;
        PreparedStatement pst = MyConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        try {
            pst.setLong(1, order.getIdCustomer());
            pst.setLong(2, order.getIdStatus());
            pst.setString(3, order.getOrderDate());
            pst.setLong(4, order.getIdPaymentType());
            pst.setLong(5, order.getIdAddress());
            pst.setLong(6, order.getIdApplication());

            if (pst.executeUpdate() > 0) {
                ResultSet generatedKeys = pst.getGeneratedKeys();
                if (generatedKeys.next()) {
                    output = generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Não foi possível cadastrar o pedido.");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return output;

    }

    public List<OrderModel> orders(OrderModel order) {
        String sql = "select p.idPedido, descStatus, convert(varchar(10), dataPedido, 103) as dataPedido, \n"
                + "(SELECT sum(ip.qtdProduto * ip.precoVendaItem) FROM ItemPedido as ip where ip.idPedido = p.IdPedido) as valorTotal\n"
                + "from pedido as p\n"
                + "join StatusPedido as sp on sp.idStatus = p.idStatus\n"
                + "join TipoPagamento as tp on tp.idTipoPagto = p.idTipoPagto\n"
                + "where p.idCliente = ?";
        List<OrderModel> result = new ArrayList<>();

        try {
            PreparedStatement pst = MyConnection.getPreparedStatement(sql);
            pst.setLong(1, order.getIdCustomer());
            ResultSet res = pst.executeQuery();

            while (res.next()) {
                OrderModel item = new OrderModel();
                item.setId(res.getLong("idPedido"));
                item.setDescStatus(res.getString("DescStatus"));
                item.setOrderDate(res.getString("dataPedido"));
                item.setTotalPrice(res.getLong("valorTotal"));

                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return result;

    }

    public List<OrderItemModel> getOrder(Long id) throws SQLException {
        String sql = "select pr.idProduto, pr.nomeProduto as nome, ip.qtdProduto as qtd, ip.precoVendaItem * ip.qtdProduto as total\n"
                + "from pedido as p\n"
                + "join itemPedido as ip on ip.idPedido = p.idPedido\n"
                + "join produto as pr on pr.idProduto = ip.idProduto where p.idPedido = ?";

        List<OrderItemModel> result = new ArrayList<>();

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        try {

            pst.setLong(1, id);
            ResultSet res = pst.executeQuery();

            while (res.next()) {
                OrderItemModel item = new OrderItemModel();
                item.setId(res.getLong("idProduto"));
                item.setName(res.getString("nome"));
                item.setQtd(res.getInt("qtd"));
                item.setPrice(res.getString("total"));
                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;

    }

}
