package dao;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.ProductModel;

public class ProductDAO {

    private String defaultSelect = "nomeProduto, idProduto, precProduto, descProduto, descontoPromocao, idCategoria, ativoProduto, idUsuario, qtdMinEstoque";

    public List<ProductModel> getProducts() throws SQLException {

        String sql = "SELECT " + this.defaultSelect + " FROM produto";
        List<ProductModel> result = new ArrayList<>();

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        try {

            ResultSet res = pst.executeQuery();
            while (res.next()) {
                ProductModel item = new ProductModel();
                item.setId(res.getInt("idProduto"));
                item.setName(res.getString("nomeProduto"));
                item.setPrice(res.getString("precProduto"));
                item.setDescription(res.getString("descProduto"));
                item.setDiscountPromotion(res.getString("descontoPromocao"));
                item.setCategoryId(res.getInt("idCategoria"));
                item.setActiveProduct(res.getInt("ativoProduto"));
                item.setMinimumStockQuantity(res.getInt("qtdMinEstoque"));
                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;

    }

    public ProductModel getProductById(ProductModel productId) {
        String sql = "SELECT " + this.defaultSelect + " FROM produto WHERE idProduto=?";
        ProductModel item = null;

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        try {

            pst.setInt(1, productId.getId());
            ResultSet res = pst.executeQuery();

            if (res.next()) {
                item = new ProductModel();
                item.setId(res.getInt("idProduto"));
                item.setName(res.getString("nomeProduto"));
                item.setPrice(res.getString("precProduto"));
                item.setDescription(res.getString("descProduto"));
                item.setDiscountPromotion(res.getString("descontoPromocao"));
                item.setCategoryId(res.getInt("idCategoria"));
                item.setActiveProduct(res.getInt("ativoProduto"));
                item.setMinimumStockQuantity(res.getInt("qtdMinEstoque"));

            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return item;

    }

    public List<ProductModel> getProductsByCategory(ProductModel product) {
        String sql = "SELECT " + this.defaultSelect + " FROM produto WHERE idCategoria = ?";
        List<ProductModel> result = new ArrayList<>();

        try {

            PreparedStatement pst = MyConnection.getPreparedStatement(sql);
            pst.setInt(1, product.getCategoryId());
            ResultSet res = pst.executeQuery();

            while (res.next()) {
                ProductModel item = new ProductModel();
                item.setId(res.getInt("idProduto"));
                item.setName(res.getString("nomeProduto"));
                item.setPrice(res.getString("precProduto"));
                item.setDescription(res.getString("descProduto"));
                item.setDiscountPromotion(res.getString("descontoPromocao"));
                item.setCategoryId(res.getInt("idCategoria"));
                item.setActiveProduct(res.getInt("ativoProduto"));
                item.setMinimumStockQuantity(res.getInt("qtdMinEstoque"));
                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return result;

    }
    
    public byte[] getProductImage(Long id){
        String sql = "SELECT imagem FROM produto WHERE idProduto = ?";
        
        byte[] image = new byte[1];
        
        try {

            PreparedStatement pst = MyConnection.getPreparedStatement(sql);
            pst.setLong(1, id);
            ResultSet res = pst.executeQuery();
            
            while(res.next()){
                image = res.getBytes("imagem");
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return image;
    }
    
    
    public List<ProductModel> search(String parameter) throws SQLException {

        String sql = "SELECT " + this.defaultSelect + " FROM produto WHERE nomeProduto LIKE ? OR descProduto LIKE ? OR idProduto LIKE ?";
        List<ProductModel> result = new ArrayList<>();

        PreparedStatement pst = MyConnection.getPreparedStatement(sql);
        pst.setString(1, "%" + parameter + "%");
        pst.setString(2, "%" + parameter + "%");
        pst.setString(3, "%" + parameter + "%");
        
        try {

            ResultSet res = pst.executeQuery();
            while (res.next()) {
                ProductModel item = new ProductModel();
                item.setId(res.getInt("idProduto"));
                item.setName(res.getString("nomeProduto"));
                item.setPrice(res.getString("precProduto"));
                item.setDescription(res.getString("descProduto"));
                item.setDiscountPromotion(res.getString("descontoPromocao"));
                item.setCategoryId(res.getInt("idCategoria"));
                item.setActiveProduct(res.getInt("ativoProduto"));
                item.setMinimumStockQuantity(res.getInt("qtdMinEstoque"));

                result.add(item);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return result;

    }
    
}
