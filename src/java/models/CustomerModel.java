package models;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

public class CustomerModel {

    private Long id;
    private String email;
    private String name;
    private String password;
    private String cpf;
    private String cell_phone;
    private String commercial_phone;
    private String residencial_phone;
    private String birth;
    private String send_newsletter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = sha1(password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public String getCell_phone() {
        return cell_phone;
    }

    public String getCommercial_phone() {
        return commercial_phone;
    }

    public String getResidencial_phone() {
        return residencial_phone;
    }

    public String getBirth() {
        return birth;
    }

    public String getSend_newsletter() {
        return send_newsletter;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setCell_phone(String cell_phone) {
        this.cell_phone = cell_phone;
    }

    public void setCommercial_phone(String commercial_phone) {
        this.commercial_phone = commercial_phone;
    }

    public void setResidencial_phone(String residencial_phone) {
        this.residencial_phone = residencial_phone;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public void setSend_newsletter(String send_newsletter) {
        this.send_newsletter = send_newsletter;
    }

    public String sha1(String password) { 
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(password.getBytes("UTF-8"), 0, password.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            Logger.getLogger(Encriptacion.class.getName()).log(Level.SEVERE, null, e);
        }
        return sha1;
    }

}
