package models;

public class ProductModel {

    private Integer id;
    private String name;
    private String price;
    private String description;
    private String discountPromotion;
    private Integer activeProduct;
    private Integer minimumStockQuantity;
    private Integer categoryId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getActiveProduct() {
        return activeProduct;
    }

    public void setActiveProduct(Integer activeProduct) {
        this.activeProduct = activeProduct;
    }

    public Integer getMinimumStockQuantity() {
        return minimumStockQuantity;
    }

    public void setMinimumStockQuantity(Integer minimumStockQuantity) {
        this.minimumStockQuantity = minimumStockQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setDiscountPromotion(String discountPromotion) {
        this.discountPromotion = discountPromotion;
    }

    public String getDiscountPromotion() {
        return discountPromotion;
    }

}
