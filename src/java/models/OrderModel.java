package models;

import java.util.ArrayList;

public class OrderModel {
    
    private Long id;
    private Long idStatus;
    private String DescStatus;
    private String orderDate;
    private Long idPaymentType;
    private Long idAddress;
    private Long idApplication;
    private Long idCustomer;
    private Long totalPrice;
    private ArrayList<OrderItemModel> products;

    public ArrayList<OrderItemModel> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<OrderItemModel> products) {
        this.products = products;
    }

    public String getDescStatus() {
        return DescStatus;
    }

    public void setDescStatus(String DescStatus) {
        this.DescStatus = DescStatus;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
   
    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Long idStatus) {
        this.idStatus = idStatus;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Long getIdPaymentType() {
        return idPaymentType;
    }

    public void setIdPaymentType(Long idPaymentType) {
        this.idPaymentType = idPaymentType;
    }

    public Long getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(Long idAddress) {
        this.idAddress = idAddress;
    }

    public Long getIdApplication() {
        return idApplication;
    }

    public void setIdApplication(Long idApplication) {
        this.idApplication = idApplication;
    }

    
  
    
}